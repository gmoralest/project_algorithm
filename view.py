# coding: utf-8

import os
from tkinter import Button
from tkinter import Entry
from tkinter import Label
from tkinter import Checkbutton
from tkinter import StringVar
from tkinter import IntVar
from tkinter import Tk
from tkinter import Canvas
from tkinter import filedialog 

"""

@author : Gabriel Morales

"""

class View(Tk):
    def __init__(self, controller):
        super().__init__()
        self.controller = controller
        self.widgets_labs = {} # save the id of the created labels for later acces
        self.widgets_entry = {} # save the id of the created entries for later acces
        self.entries_var = {} # save the id of the created variables for later acces
        self.widgets_button = {} # save the id of the created butttons for later acces
        self.widgets_canvas = {} # save the id of the created canvas for later acces
        self.canvas = ["display_canvas"] # list of canvas to be created 
        self.entries = ["BW Transformation","BW Retransformation", "Huffman Compression","Huffman Decompression"] # list of cheackboxes to be created
        self.buttons = ["Next","Last", "Save"] # list of buttons to be created

    def get_value(self, key):
        """Returns the value of the entry box"""
        return self.view.widgets_entry[key].get()
    

    def create_fields(self):
        """from the previulsy created lists, it creates the differents Tkinter objects and places into the frame"""
        lab = Label(self, text = " Burrows Weeler Transformation \\\ Huffman compression\n ",font=("Arial", 25))
        self.widgets_labs["Subtitle"] = lab
        lab.grid(row=0,column=0, columnspan=4, sticky="n")

        lab = Label(self, text="ENTER A SEQUENCE")
        self.widgets_labs["Enter"] = lab
        lab.grid(row=1,column=1, sticky="e")
        
        self.entry_var = StringVar()
        entry = Entry(self,width = 30, textvariable = self.entry_var)
        self.widgets_labs["Entry"] = entry
        entry.grid(row=1,column=2)

        buttonW = Button(self, text = "Enter", command=(lambda button="Enter": self.controller.button_press_handle(button))) 
        self.widgets_button["Enter"] = buttonW
        buttonW.grid(row=1,column=3, padx=15, pady=15, sticky="w")
        
        lab = Label(self, text="OR SELECT A FILE FROM YOUR COMPUTER") 
        self.widgets_labs["Select"] = lab
        lab.grid(row=2,column=2, sticky="e")
        
        buttonW = Button(self, text = "Open", command=(lambda button="Open": self.controller.button_press_handle(button)))
        self.widgets_button["Open"] = buttonW
        buttonW.grid(row=2,column=3, padx=15, pady=15, sticky="w")

        lab = Label(self, text="")
        self.widgets_labs["status"] = lab
        lab.grid(row=9,column=1, columnspan=2)
        
        
        for i in self.canvas:
            canvas = Canvas(self,bg="white", height=300, width=400)
            canvas.grid(row=3,column=1,columnspan=2, rowspan = 5, padx=15, pady=15)
            self.widgets_canvas[i] = canvas

        i, j  = 0, 1
  
        for idi in self.buttons:
            
            buttonW = Button(self, text = idi, command=(lambda button=idi: self.controller.button_press_handle(button))) # creation of buttons, each one with is assigned fucntion from the controller
            self.widgets_button[idi] = buttonW
            buttonW.grid(row=i+13,column=j, padx=15, pady=15)

            j += 1

        for idi in self.entries:
            lab = Label(self, text=idi.capitalize())
            self.widgets_labs[idi] = lab
            lab.grid(row=i+3,column=3, sticky="w")

            var = IntVar()
            self.entries_var[idi] = var
            entry = Checkbutton(self, variable=var, command= (lambda button=idi: self.controller.checkbox_select_off_on(button))) # creation of the checkboxes. that have as an assigned command the enable/disable of all the rest of the checkbox
            self.widgets_entry[idi] = entry
            entry.grid(row=i+3,column=2, sticky="e")
            self.widgets_entry[idi].config(state="disabled") # all the checkboxes are created disabled, to be activaded according to the sequence type

            i += 1
    
    
    def uploadaction(self):
        """ Display a popup that asks the users for the file that he wants to upload and returns the filename, and the extension"""
        path = filedialog.askopenfilename()
        if len(path) == 0: # asksaveasfile return `None` if dialog closed with "cancel".
            return None, None
        filename, file_extension = os.path.splitext(path)
        return filename, file_extension
    

    def retrieve_input(self):
        """Recover the text from the Entry box"""
        return self.entry_var.get()
    

    def popupmsg(self,msg):
        """Display a popup message that display the entered message"""
        popup = Tk()
        popup.wm_title("Warning")
        label = Label(popup, text=msg)
        label.pack(side="top", fill="x", pady=10)
        B1 = Button(popup, text="Ok", command = popup.destroy)
        B1.pack()
        popup.mainloop()
    
    
    def save_action(self):
        """opens a dialog box that askk the user for the save file name and path"""
        save_path = filedialog.asksaveasfile(mode='w', defaultextension=".txt")
        if save_path is None: # asksaveasfile return `None` if dialog closed with "cancel".
            return
        return save_path



    def change_file_label(self, text):
        """changes the status of the selected file label"""
        self.widgets_labs["Select"]['text'] = text

    
    def clean_canvas_display(self):
        """ Deletes all the text from the Tkinter canvas """
        self.widgets_canvas["display_canvas"].delete("display_canvas_text")
    

    def canvas_display(self, display_text):
        """ Function for display simple string objects in the Tkinter canvas"""
        self.widgets_canvas["display_canvas"].create_text(200,150, text = display_text, tag="display_canvas_text")


    def canvas_display_tkinter_bwt(self, display_text):
        """ Function for display list objects in the Tkinter canvas that correspond to the results from the Burrows Weeler transformation"""
        if isinstance(display_text,list):
            text = ""
            for i in display_text:
                i = i + "\n"
                text += i
            self.widgets_canvas["display_canvas"].create_text(200,150, text = text, tag="display_canvas_text",font=("Arial", 8)) # 
        else:
            self.widgets_canvas["display_canvas"].create_text(200,150, text = display_text, tag="display_canvas_text",font=("Arial", 8))
    
    def canvas_display_tkinter_huffman(self, display_text):
        """ Function for display list objects in the Tkinter canvas that correspond to the results from the Huffman compression"""
        if isinstance(display_text,dict):
            text = ""
            
            for i in display_text.items():
                line = str(i[0]) + str(i[1]) + "\n"
                text += line
            self.widgets_canvas["display_canvas"].create_text(200,150, text = text, tag="display_canvas_text",font=("Arial", 8))
        else:
            self.widgets_canvas["display_canvas"].create_text(200,150, text = display_text, tag="display_canvas_text",font=("Arial", 8))
        
    def all_checkbox_off(self):
        """Disable all the checkboxes"""
        for entry in self.widgets_entry:
            self.widgets_entry[entry].deselect()
            self.widgets_entry[entry].config(state="disabled")

    def checkbox_off(self, entries):
        """Deselect an specific checkbox"""
        self.widgets_entry[entries].deselect()
        self.widgets_entry[entries].config(state="disabled")
    
    def checkbox_on(self, entries):
        """Enable an specific checkbox"""
        self.widgets_entry[entries].config(state="normal")

    
    def clear_entry_text(self):
        """clear the entry text widget"""
        self.widgets_labs["Entry"].delete(0, "end")


    def main(self):
        """initializes the view object"""
        self.title("Transformée de Burrows Weeler - Compression d’Huffman")
        self.mainloop()
