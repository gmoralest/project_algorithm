## BWT - HC project

## Name
Burrows Weeler Transformation - Huffman compression algorithm Project

## Description
This program allows the user to perform a Burrows Weeler transformation and/or a Huffman compression of a selected sequence, which can be entered by the user in file.TXT format or by entering through the keyboard. Depending on the users choice, the program allows to return the different steps for the Burrows Weeler (re)transformation, and in the case of the Huffman compression it returns the variables used to perform the operation ("Original Sequence","Binary sequence","Character sequence","Codes Dictionary"). Finally the user can choose to save the result of the performed operation.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

- [ ] [Add files using the command line] pull repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gmoralest/project_algorithm.git
git pull https://gitlab.com/gmoralest/project_algorithm.git

```

## Usage
Usage by entry text
1) get into the existing_repository where all the files are stored
2) open a terminal 
3) python3 controller.py 
4) Enter a DNA sequence in the Entry text box
5) Press enter
- The entre sequence will be presented in the screen
6) You have to select one of the operation presented on the left side ["BW Transformation","BW Retransformation", "Huffman Compression","Huffman Decompression"]
- Once you select one of the option, the other options would be desabled
7) Press the button "Last"
- The result would be presented in the screen
8) For a detailed description of the steps , press "next"
9) you could repeat the step 3 for perform different options
10) For save the Result of the last performed operation , press " Save"
- it will open a dialog box, for select the saving path and the file name

Usage by file
1) get into the existing_repository where all the files are stored
2) open a terminal 
3) python3 controller.py 
4) Enter a DNA sequence in a TXT.file ( the file must contain the .TXT extension)
- The entre sequence will be presented in the screen
5) You have to select one of the operation presented on the left side ["BW Transformation","BW Retransformation", "Huffman Compression","Huffman Decompression"]
- Once you select one of the option, the other options would be desabled
6) Press the button "Last"
- The result would be presented in the screen
7) For a detailed description of the steps , press "next"
8) you could repeat the step 3 for perform different options
9) For save the Result of the last performed operation , press " Save"
- it will open a dialog box, for select the saving path and the file name

## Support
For any question or suggestion about this project you can write me at gabriel-alejandro.MORALES-TAPIA@etu.univ-amu.fr


## Authors and acknowledgment
Gabriel Morales Tapia
M1 DLAD mastes student - Aix-Marseille Université

## Project status
Delivered and post presentation improvement
