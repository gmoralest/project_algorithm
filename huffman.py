# coding: utf-8

import heapq

"""

@author : Gabriel Morales

"""

class HeapNode:
    def __init__(self, char, freq):
      """ initializes the node object class"""
      self.char = char
      self.freq = freq
      self.left = None
      self.right = None

    def __lt__(self, other):
      return self.freq < other.freq

    def __eq__(self, other):
      if(other == None):
        return False
      if(not isinstance(other, HeapNode)):
        return False
      return self.freq == other.freq

class HuffmanCoding:
    def __init__(self):
        """ initializes the huffman object for operation perform"""
        self.heap = [] # stores all the created nodes
        self.codes = {'A': None, 'T': None, 'C': None, 'G': None, 'N': None, '$': None} # stores all the generated codes to binary
        self.reverse_mapping = {} # create a version binary to character code dictionary
        self.encoded_text = None # variable that stores the final result of the compression
        self.encoded_dico = None # variable that stores the codes used for the compression
        self.compress_results = {"Original Sequence: " : None, "Binary sequence: ": None, "Character sequence: ": None, "Codes Dictionary: ": None } # list with the results from the compression
        self.decompress_results = {"Character sequence: ": None, "Binary sequence: ": None, "Original Sequence: " : None, "Reverse Codes Dictionary: ": None }# list with the results from the decompression


    def make_frequency_dict(self, text):
        """ buid frecuency dictionary """
        frequency = {}
        for character in text:
            if not character in frequency:
                frequency[character] = 0
            frequency[character] += 1
        return frequency
    
    def make_heap(self, frequency):
        """creates the nodes and build priority queue"""
        for key in frequency:
            node = HeapNode(key, frequency[key]) # create the node
            heapq.heappush(self.heap, node)  # build priority queue

    def merge_nodes(self):
        """build HuffmanTree by selecting 2 min nodes and merging"""
        while(len(self.heap) > 1):
            node1 = heapq.heappop(self.heap)
            node2 = heapq.heappop(self.heap)

            merged = HeapNode(None, node1.freq + node2.freq) # create the merged new node
            merged.left = node1 # set the pointers of the new created node
            merged.right = node2
            heapq.heappush(self.heap, merged) # put the new created node into the priority queue
    
    def make_code_helper(self, node, current_code):
      """run across the priority queue, and creates the binary code asociated to the the entered character"""
      if(node == None):
        return
      if(node.char != None):
        self.codes[node.char] = current_code # save the generated code into the code variable
        self.reverse_mapping[current_code] = node.char # creates the reversed version of the code dictionary
        return
      
      self.make_code_helper(node.left, current_code + "1")  # recursive function to re-run the functon in case that the reached node doenst have a fracuency asocieated
      self.make_code_helper(node.right, current_code + "0") # recursive function to re-run the functon in case that the reached node doenst have a fracuency asocieated
    
    def make_codes(self):
        """assign codes to characters, initiating the function from the root node"""
        root = heapq.heappop(self.heap)
        current_code = ""
        self.make_code_helper(root,current_code)
    
    def get_encoded_text(self, text):
        """encode the input text into binary"""
        enconded_text = ""
        for character in text:
          enconded_text += self.codes[character]
        return enconded_text    
    
    def pad_encoded_text(self, encoded_text):
        """if overall lenght of final encode bit streams is not multiple of 8, 
        add some padding to the text"""
        extra_padding = 8 - (len(encoded_text) % 8) # determine how many 0 it misses to reach a multiple of 8 sequence size
        for i in range(extra_padding): # add the 0 to the created binary sequence
          encoded_text += "0"
        padded_info = "{0:08b}".format(extra_padding) # creates an octet to add the nomber of 0 added to the encoded text
        encoded_text = padded_info + encoded_text # concatenate the padding info with the created binary sequence
        return encoded_text


    def get_ascii_array(self, padded_encoded_text):
        """Transform the binary sequence into a ascii characters"""
        b = ""
        for i in range(0,len(padded_encoded_text),8): # read the binary sequence octet by octet,
          byte = padded_encoded_text[i:i+8]
          b += chr((int(byte,2))) # translates the octet into an ascii character
        return b

    def get_encoded_key(self,key):
      """creates the codes for the compression code dictionary"""
      key = "1" + key # add a 1 in order to protect the sequence
      extra_padding = 8 - (len(key) % 8) #add o to the sequence in case the lenght is not multiple of 8 to be translated
      for j in range(extra_padding):
        key = "0" + key
      return key 
    
    def get_binary_dico(self, dico):
      """ run across the code dictionary to transform it into binary"""
      binary_dico = ""
      missing_letters = "ATCGN$"
      for i in dico.keys():
        if dico[i] != None : # evaluates if the key is in the code dictionary
          encoded_key = self.get_encoded_key(dico[i]) # transform the key into binary
          binary_dico += encoded_key # add the key into the binary dictionary string
          missing_letters = missing_letters.replace(i.upper(), "") # eliminates the treated key from the missing letters to create the final str with all the characters that are not in the sequence
      return binary_dico, missing_letters 

    def get_enconded_dico(self, binary_dico, missing_letters):
      """ creates the final ascii code from the codes dictionary"""
      encoded_dico = "￿" # add an special 16 bits character to indicates the begining of the compressed codes dictionary
      ascii_dico = self.get_ascii_array(binary_dico) # transform the binary string with the codes of the dictionary keys into ascii characters
      encoded_dico += ascii_dico + "￿" + missing_letters # add a new special character at the end of the codes, and concatenate the letters that are not present in to codes
      return encoded_dico


    def save_ascii_to_file(self, save_path,enconded_dico,encoded_text):
      """save the compressed dictionary and the compressed sequence"""
      save_path.write(enconded_dico + "\n")
      save_path.write(encoded_text)
      save_path.close()

    def decompress_dico(self, ascii_dico_array):
      """ decompress the compresedd codes dictionary string depending of the order of the characters"""
      ascii_dico_array = ascii_dico_array.rstrip("\n") # eliminates the space character from the sequence
      ascii_dico_array = ascii_dico_array.split("￿") # separates the sequence in the previusly added special character 
      ascii_dico_array = ascii_dico_array.replace(" ","")
      missing_letters = ascii_dico_array[2] # recover the missing letter string 
      mapping_dico = "ATCGN$" 
      m_index = 0
      char = repr(ascii_dico_array[1]).replace(r"'","" ) # creates an string of the ascci character, leaving just the hexadecimal code
      for i in  char.split(r"\x"):
        if len(i) > 1: #passes all the potencial empty characters 
          if mapping_dico[m_index] not in missing_letters:
            code = bin(int(i, 16))[2:].zfill(8) # translates the exadecimal code into binary
            code = code[code.find("1")+1:] # eliminates de previusly added 1 character (added to secure the integrity of the code and read were it starts) 
            self.codes[mapping_dico[m_index]] = code # read each position of the binary string as one predetermined character, if is on the list of missong letters, it skip and continuous to read the next character.
            m_index += 1
          else:
            m_index += 1

    def remove_padding(self, padded_encoded_text):
      """reads the information from the binary code (how many 0 has been added), and eliminates them, as long woth the padding information """
      padded_info = padded_encoded_text[:8] # reads the binary  number of 0 previulsy added
      extra_padding = int(padded_info, 2) #translate the padding information into integer
      padded_encoded_text = padded_encoded_text[8:] # eliminates the padding information
      encoded_text = padded_encoded_text[:-1*extra_padding] #eliminates the 0 added
      return encoded_text

    def decode_text(self, encoded_text):
      """translates the binary code into characters using the reversed codes."""
      current_code = ""
      decoded_text = ""
      for bit in encoded_text:
        current_code += bit
        if(current_code in self.reverse_mapping):
          character = self.reverse_mapping[current_code]
          decoded_text += character
          current_code = ""
      return decoded_text

    def decompress_from_file(self, input_path):
      """decompression function for sequence entered by file upload function, grouping all the previusly created functions, and saving all the results into the results list dictionary"""
      self.reverse_mapping = {value : key for (key, value) in self.codes.items()}
      bites_string = ""
      with open(input_path, 'r') as file:
          bit_string = ""
          file.readline()
          byte = file.read(1)
          bites_string += byte
          while(len(byte) > 0):
            byte = ord(byte)
            bits = bin(byte)[2:].rjust(8, '0')
            bit_string += bits
            byte = file.read(1)
          encoded_text = self.remove_padding(bit_string)
          decompressed_text = self.decode_text(encoded_text)
      self.decompress_results["Character sequence: "] = bites_string
      self.decompress_results["Binary sequence: "] = encoded_text
      self.decompress_results["Original Sequence: "] = decompressed_text
      self.decompress_results["Reverse Codes Dictionary: "] = self.reverse_mapping
      return decompressed_text
    
    def decompress_from_program(self):
      """decompression function for sequence entered by entry text option, grouping all the previusly created functions, and saving all the results into the results list dictionary"""
      bit_string = ""
      for byte in self.encoded_text:
        byte = ord(byte)
        bits = bin(byte)[2:].rjust(8, '0')
        bit_string += bits
      encoded_text = self.remove_padding(bit_string)
      decompressed_text = self.decode_text(encoded_text)
      self.decompress_results["Character sequence: "] = self.encoded_text
      self.decompress_results["Binary sequence: "] = encoded_text
      self.decompress_results["Original Sequence: "] = decompressed_text
      self.decompress_results["Reverse Codes Dictionary: "] = self.reverse_mapping