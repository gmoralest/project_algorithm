# coding: utf-8

from curses.ascii import isalpha
from view import View
from bwt import Transformer
from huffman import HuffmanCoding

"""

@author : Gabriel Morales

"""


class Controller():
    def __init__(self):
        self.view = View(self) #Creates an object from view.py that contains all the user interface elements
        self.Transformer = Transformer() # creates an object that contains the model for the Huffman transformation operations 
        self.HuffmanCoding = HuffmanCoding() #creates an object that contains  for the Burrows Weeler Transformation operations
        self.path = None # variable that keeps in storage the path of the loaded file
        self.current_sequence = None # variable that stores the final results of the operation selected, in order to perform the next

    
    def start_view(self):
        """ Initializes the visual interface"""
        self.view.create_fields()
        self.view.main()


    def button_press_handle(self, buttonId):
        """ Discriminate actions based on the button pressed by the user"""
        if buttonId == "Open":
            self.view.clear_entry_text() # deletes all the text from the widget Entry box previusly written by the user
            self.view.clean_canvas_display() # deletes all the text displayed in the canvas
            self.path, file_extension = self.view.uploadaction() # ask the user for the file that wants to use for the program and stores the path and the extension
            if self.path is None: # Stops the program if the cancel button is pressed in the filedialog popup from the view.
                return
            self.path = self.path + file_extension # rensemble the complete file path of the selected file
            if file_extension == ".txt": # evaluates the file extension and allows only .TXT file extension.
                self.view.change_file_label("FILE SELECTED") # changes the status of the selected file label
                self.view.all_checkbox_off() # disable all the checkbox, or order to enable them according to the sequence type. 
                sequence, ascii_dico_array = self.sequence_lecture(self.path) #proceeds to read the file and extract the sequence and the diccionary(if there is one)
                if ascii_dico_array != None: # evaluates the existense of a dictionary
                    self.current_sequence = sequence # indicates the sequence with which the program is going to work
                    self.view.canvas_display(sequence) # display the sequence in the screen canvas
                    self.view.checkbox_on("Huffman Decompression") # if there is a dictionary, it means that is compressed, so enables the decompression button
                else:
                    self.current_sequence = sequence # indicates the sequence with which the program is going to work
                    self.view.canvas_display(sequence)# display the sequence in the screen canvas
                    self.file_bwt_identification(sequence) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                    self.view.checkbox_on("Huffman Compression") # if there is no dictionary, it means that is a decompressed sequence, so enables the compression button
            else:
                self.view.popupmsg("you must upload a file containing a .txt extension") # Error message in case that the user enter a file with an invalid extension
        elif buttonId == "Enter": # Button that allow to process the information in the Entry box from the users interface
            entry_text = self.view.retrieve_input() # Recover the text from the Entry box and stores the result in a variable
            entry_text = entry_text.upper() # Transform the characters of the string in uppercase
            if entry_text == "":# Evaluates if an empty string is entered in the field 
                self.view.popupmsg("You must enter a sequence")# Error message in case that the user enter no text in the entry box
                return
            else:
                self.view.clean_canvas_display() # deletes all the text displayed in the canvas
                self.view.clear_entry_text() # deletes all the text from the widget Entry box previusly written by the user
                file_type = self.file_bwt_identification(entry_text) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                if file_type == "bwt_yes":
                    valid_seq = self.bwt_sequence_test(entry_text) # Perform a test in order to evaluates if the entered sequence is a valid BWT sequence
                    if valid_seq:
                        self.current_sequence = entry_text # indicates the sequence with which the program is going to work
                        self.view.canvas_display(entry_text) # display the sequence in the screen canvas
                    else:
                        self.view.widgets_labs["status"]['text'] = "" # deletes the text of the status (type of sequence) label
                        self.view.popupmsg("You must enter a valid sequence") # Error message in case that the user entered a BWT invalid sequence
                elif file_type == "bwt_no":
                    self.current_sequence = entry_text  # indicates the sequence with which the program is going to work
                    self.view.canvas_display(self.current_sequence) # display the sequence in the screen canvas

        elif buttonId == "Next":
            if self.current_sequence == None: # Test to evaluate if there is a selected file or entered sequence to work with
                self.view.popupmsg("You must ENTER a sequence or OPEN a file") 
            else:
                if len(self.Transformer.results_matrix_reconstruction) == 0  and  len(self.Transformer.results_matrix_construction) == 0: # Evaluates if a BWT operations has been performed in order to display all the previous steps before the result.
                    self.view.popupmsg("You must perform an BWT operation")      
                else:    
                    self.view.clean_canvas_display()
                    if len(self.Transformer.results_matrix_reconstruction) != 0: # evaluates if the results correspond to  a BWT transformation,
                        while len(self.Transformer.results_matrix_reconstruction) > 0: # Shows the results contained in the list of matrix from the BWT object generated when the compression operation is performed
                            try:
                                self.view.canvas_display_tkinter_bwt(self.Transformer.results_matrix_reconstruction[0]) # display the first element in the list of matrix results 
                                self.Transformer.results_matrix_reconstruction.pop(0) # deletes the fist element, already displayed in screen from the list
                                return
                            except IndexError:
                                self.view.canvas_display_tkinter_bwt(self.Transformer.result) # in case that already show all the results  ( and therefore the list of matrix results is empty), display the final result
                                return
                    else:
                         while len(self.Transformer.results_matrix_construction) > 0: # Shows the results contained in the list of matrix from the BWT object generated when the decompression operation is performed
                            try:
                                self.view.canvas_display_tkinter_bwt(self.Transformer.results_matrix_construction[0])  # display the first element in the list of matrix results 
                                self.Transformer.results_matrix_construction.pop(0) # deletes the fist element, already displayed in screen from the list
                                return
                            except IndexError:
                                self.view.canvas_display_tkinter_bwt(self.Transformer.result) # in case that already show all the results  ( and therefore the list of matrix results is empty), display the final result
                                return
        elif buttonId == "Last": # initializes the operations to perform the selected option from the checkbox.
            if self.current_sequence == None:# Test to evaluate if there is a selected file or entered sequence to work with
                self.view.popupmsg("You must ENTER a sequence or OPEN a file")
            else:
                self.Transformer.results_matrix_construction = [] #Empty the previusly generated results from the operations performed before..
                self.Transformer.results_matrix_reconstruction = []
                check_box_values = {} 
                for i in self.view.entries_var: #recover the selected options from the checkboxes and store the result in a dictionary.
                    check_box_values[i] = self.view.entries_var[i].get()
                if all(values != 0 for values in check_box_values.values()): # evaluates if there is a selected result
                    self.view.popupmsg("You must select a an option")
                    return    
                self.view.clean_canvas_display() # deletes all the text displayed in the canvas
                if check_box_values['BW Retransformation'] == 1: # evaluates if the selected option is BW Reransformation
                    sequence_bwt_list = self.Transformer.retransformation(self.current_sequence) # Perform all the operation for the BWT retransformation and stores all the results in a list
                    sequence_bwt_retransformed = self.Transformer.recover_sequence(sequence_bwt_list) # from that list, it recovers the FINAL bwt result
                    self.view.widgets_labs["status"]['text'] = "BW Retransformation Performed" # changes the Status label
                    self.current_sequence = sequence_bwt_retransformed # Stores the trasnformed sequence string as the current working sequence
                    self.view.canvas_display(sequence_bwt_retransformed) # display the final result in the canvas
                    self.view.checkbox_off('BW Retransformation') # disable checkbox button for the BW Retransformation
                    self.file_bwt_identification(self.current_sequence) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                if check_box_values['BW Transformation'] == 1: # evaluates if the selected option is BW Transformation
                    sequence_list = self.Transformer.bwt_transformation(self.current_sequence) # Perform all the operation for the BWT transformation and stores all the results in a list
                    sequence_bwt = self.Transformer.bwt_selection(sequence_list) # from that list, it recovers the FINAL bwt result
                    self.view.widgets_labs["status"]['text'] = "BW Transformation Performed"  # changes the Status label
                    self.current_sequence = sequence_bwt # Stores the trasnformed sequence string as the current working sequence
                    self.view.canvas_display(sequence_bwt)  # display the final result in the canvas
                    self.view.checkbox_off('BW Transformation')# disable checkbox button for the BW Transformation
                    self.file_bwt_identification(self.current_sequence) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                if check_box_values['Huffman Compression'] == 1: # evaluates if the selected option is Huffman Compression
                    frequency = self.HuffmanCoding.make_frequency_dict(self.current_sequence) #buid frecuency dictionary and stores the variable
                    self.HuffmanCoding.make_heap(frequency) # creates the nodes with the previusly created dictionary
                    self.HuffmanCoding.merge_nodes() # it perform the summatory of all the nodes, merging the 2 smaller values together, creating a node tree
                    self.HuffmanCoding.make_codes() # based on the node tree, ot creates the codes for sequence translation
                    encoded_text = self.HuffmanCoding.get_encoded_text(self.current_sequence) # translate the sequence into binary using the previusly generated codes
                    padded_encoded_text = self.HuffmanCoding.pad_encoded_text(encoded_text) # if the text doesnt have the correct lenght (multiple of 8), it add the corresponding 0 at the end of the string to reach the size, and add an extra octet to indicate the number of o added to the sequence.
                    ascii_array = self.HuffmanCoding.get_ascii_array(padded_encoded_text) # it transform the binary sequence in Ascii Characters
                    binary_dico, missing_letters = self.HuffmanCoding.get_binary_dico(self.HuffmanCoding.codes) #Transform the previusly created dictionary codes into binary
                    ascii_dico_array = self.HuffmanCoding.get_enconded_dico(binary_dico, missing_letters) # # it transform the binary sequence in Ascii Characters.
                    self.HuffmanCoding.encoded_text = ascii_array # Stores the final result in the  Huffman objetc.
                    self.HuffmanCoding.encoded_dico = ascii_dico_array # Stores the dictionary with the codes used for obtain the final result in the  Huffman objetc.
                    self.view.checkbox_off('Huffman Compression')  # Disable the Huffman compresion checkbox, the current performing operation
                    self.HuffmanCoding.compress_results["Original Sequence: "] = self.current_sequence # Stores the final result in the  result dictionary, in order to display them in the .
                    self.HuffmanCoding.compress_results["Binary sequence: "] = encoded_text
                    self.HuffmanCoding.compress_results["Character sequence: "] = ascii_array
                    self.HuffmanCoding.compress_results["Codes Dictionary: "] = self.HuffmanCoding.codes
                    self.current_sequence = ascii_array # Stores the Ascii characters string as the current working sequence
                    self.ascii_character_verification(self.current_sequence) # verifies if the final result doesnt contains alphanumeric charachter, and if it doenst, enables the huffman decompression option from the Checkboxes.
                    self.view.canvas_display_tkinter_huffman(self.HuffmanCoding.compress_results) # Display the dictionary results in the canvas.
                if check_box_values['Huffman Decompression'] == 1: # evaluates if the selected option is Huffman Compression
                    if self.path is not None: # evaluates if the entered sequence comes from a file or is a previusly generated in use sequence from the program 
                        sequence, ascii_dico_array = self.sequence_lecture(self.path) # reads the file and recovers the compressed sequence and compressed dictionary with the compression codes
                        self.HuffmanCoding.decompress_dico(ascii_dico_array) #decompress the dictionary for using it
                        decoded_text = self.HuffmanCoding.decompress_from_file(self.path) # decompress the sequence directly readinf the file using the previusly decompressed codes dictionary.
                        self.view.canvas_display_tkinter_huffman(self.HuffmanCoding.decompress_results) # Display the results in the Tkinter canvas
                        self.file_bwt_identification(decoded_text) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                        self.view.checkbox_off('Huffman Decompression') # Disable the Huffman decompresion checkbox, the current performing operation
                        self.current_sequence = decoded_text # Stores the decompressed sequence as the current working sequence
                        self.path = None # deletes the path so that doesnt interferes with the next operations
                    else:
                        self.HuffmanCoding.decompress_from_program() # # decompress the sequence using previusly created variables 
                        self.view.canvas_display_tkinter_huffman(self.HuffmanCoding.decompress_results)  # Display the results in the Tkinter canvas
                        self.current_sequence = self.HuffmanCoding.decompress_results["Original Sequence: "]  # Stores the decompressed sequence as the current working sequence
                        self.file_bwt_identification(self.current_sequence) # identifies the type of sequence and Enables the Checkbox with the allowed operations for the type of sequence
                        self.view.checkbox_off('Huffman Decompression') # Disable the Huffman decompresion checkbox, the current performing operation
        elif buttonId == "Save":
            if self.current_sequence == None: # Evaluates if there is a current working sequence to save
                self.view.popupmsg("You must ENTER a sequence or OPEN a file")
            else:
                if self.HuffmanCoding.encoded_text is None and self.Transformer.result is None: # evaluates if an operation has been performed
                    self.view.popupmsg("You must perform an operation")
                else:    
                    save_path = self.view.save_action() # diaplay a screen to ask the user for the saving path and stores it in a variable.
                    if self.HuffmanCoding.encoded_text is not None: # evaluates if the selected 
                        self.HuffmanCoding.save_ascii_to_file(save_path, self.HuffmanCoding.encoded_dico,self.HuffmanCoding.encoded_text) # calls the saving function for the compressed file
                    else:
                        self.Transformer.save_sequence_to_file(save_path, self.Transformer.result) # calls the saving function for the decompressed or sequence
        else:
            pass


    def sequence_lecture(self,path):
        """ Read the sequence from the file and returns the sequence as a string, and a dictionary, if there is one, otherwise, it returns none for the dictionary variable"""
        with open(path, 'r') as file:
            ascii_dico_array = file.readline() # save the first line and stores it as the encoded dictionary varible
            if ascii_dico_array[0] == "￿": # search for the special character ￿ that indicates the begining of the compressed dictionary containing the codes
                sequence = file.read() # reads all the next lines in the file, and stores it as sequence
                sequence = sequence.rstrip()
            else:
                ascii_dico_array = None # if there is no special character, it means that the sequence is not compressed, so it set the dictionary as none 
                file.seek(0, 0) # restart reading the file from the beggining
                sequence = file.read() # reads all the next lines in the file, and stores it as sequence
                sequence = sequence.rstrip()
            file.close()
            return sequence, ascii_dico_array


    def file_bwt_identification(self,sequence):
        """ Evaluates if the entered sequence is a valid sequence  (contains the allowed character), 
        and change the status of the checkboxs  with the available option for that type of sequence
        finally, it returns the file_type"""
        allowed_sequence_letters = ["a","t","c","g","n","A","T","C","G","N","$"]
        file_type = "bwt_no"
        if all([c in allowed_sequence_letters for c in sequence]): # evaluates if all the variables in the sequence are in the allowed sequence letters list
            for i in sequence:
                if i == "$": # evaluates if there is a $ character to determine if is a transformed sequence
                    file_type = "bwt_yes"                 
        else:
            self.view.popupmsg("You must enter a valid sequence") # Error message in case the entered sequence is an invalid sequence.   
        if file_type == "bwt_yes":
            self.view.widgets_labs["status"]['text'] = "Transformed sequence - not compressed" # CHange the status Label with the type of the sequence
            self.view.checkbox_on("BW Retransformation") # enables the checkbox option for this specific sequence type
            self.view.checkbox_on('Huffman Compression')
        
        elif file_type == "bwt_no":
            self.view.widgets_labs["status"]['text'] = "Normal sequence - not compressed" # CHange the status Label with the type of the sequence
            self.view.checkbox_on("BW Transformation") # enables the checkbox option for this specific sequence type
            self.view.checkbox_on('Huffman Compression')
        return file_type # return the file type.


    def ascii_character_verification(self,sequence):
            """Evaluates if the sequence contains only alphanumeric characters, and if not, assume that is a compressed sequence, and finally, enables the Huffman Decompression option """
            if all(isalpha(c) for c in sequence):
                    pass
            else:
                self.view.checkbox_on('Huffman Decompression')

    
    def bwt_sequence_test(self,entry_text):
        """Performs a test to see if the entered  (That contains a $ characters) is valid sequence, and the deletes all the genereted results"""
        test_result = self.Transformer.retransformation(entry_text)
        test_result = self.Transformer.recover_sequence(test_result)
        if "$" in test_result: # the normal transformation value shouldn't contains $ character, so if it does, assume that is no a valid BWT sequence.
            self.Transformer.result = None # restores all the values used for the test to the beggining
            self.Transformer.results_matrix_construction = []
            return False
        else: 
            self.Transformer.result = None # restores all the values used for the test to the beggining
            self.Transformer.results_matrix_construction = []
            return True

    
    def checkbox_select_off_on(self, stay_on_checkbox):
        """ Function that allows to select only one option from the checkboxes (once one option is selected). When the box is unselected, it presents the user all the operation options previusly disabled. """
        if self.view.entries_var[stay_on_checkbox].get(): # it recovers the value of the selected checkbox, in order to know which one was pressed.
            for i in self.view.widgets_entry: # disable all the checkbox, except for the selected one
                if i != stay_on_checkbox:
                    self.view.widgets_entry[i].config(state="disabled")
        else: 
            self.file_bwt_identification(self.current_sequence) # this 2 functions, enables the selected boxes according to the sequence type.
            self.ascii_character_verification(self.current_sequence)


if __name__ == "__main__":
    controller = Controller()
    controller.start_view()