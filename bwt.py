# coding: utf-8

"""

@author : Gabriel Morales

"""


class Transformer():
    def __init__(self):
        self.result = None # Stores the final result 
        self.results_matrix_reconstruction = [] # Stores all the steps of the BWT transformation
        self.results_matrix_construction = [] # Stores all the steps of the BWT retransformation
        
    def bwt_transformation(self, sequence):    
        sequence += "$" # add the $ charachter into the sequence 
        seq_list = [] # generates an empty list
        seq_list.append(sequence) # add the sequence into the list
        self.results_matrix_reconstruction.append(seq_list.copy()) # stores the value an the list of results
        for i in range(0, len(sequence)-1):
            seq_temp = seq_list[i][(len(sequence)-1)] + seq_list[i] #  take the last character and place a copy of it at the begining of the sequence
            seq_temp = seq_temp[:-1] #deletes the last character
            seq_list.append(seq_temp) # saves the generated sequence into a list 
            self.results_matrix_reconstruction.append(seq_list.copy()) # save the current list into the results matrix list, step by step
        return seq_list #return the complete list

    def bwt_selection(self, seq_list):    
        """From the previusly generated list, its recover the last charachter from each sequence and concatenate for create the BWT sequence"""
        bwt = ""
        self.results_matrix_reconstruction.append(sorted(seq_list).copy())  # save the current list into the results matrix list, step by step
        for i in sorted(seq_list): # select the last character of the sorted list
            bwt += i[(len(seq_list)-1)]
        self.result = bwt # save the sequence as the final result
        self.results_matrix_reconstruction.append(bwt) # save the generated into the results matrix list
        return bwt


    def retransformation(self, bwt):
        self.results_matrix_construction.append(list(bwt).copy()) # stores the entering compressed sequence into the results list matrix
        reconstruction = sorted(list(bwt)) # separates all the sorted charachters from the sequence into a list
        self.results_matrix_construction.append(reconstruction.copy()) # enter the previusly create dlist into the results list matrix
        for j in range(0, len(bwt)):
            for i in range(0, len(bwt)):
                reconstruction[i] = list(bwt)[i] + reconstruction[i] # add one of the character from the original unsorted listo to the prevously sorted list charchaters
            if all(len(reconstruction[index]) == (j+2) for index in range(0,len(reconstruction))): # once all the values of the list have the same lenght that the number of the sequence it return the current list 
                self.results_matrix_construction.append(reconstruction.copy()) # enter the previusly create dlist into the results list matrix
                self.results_matrix_construction.append(sorted(reconstruction).copy()) # enter the previusly sorted create dlist into the results list matrix
            if j == (len(bwt)-2): # once it reaches the lenght of the original sequence ir retur a list with all the last results  
                return reconstruction
            else:
                reconstruction = sorted(reconstruction) # if it havent reach the total lenght of the original sequence, it cointinues to sort the list.



    def recover_sequence(self, reconstruction):
        """ from the previusly created list, it recover the sequence which ends with $, eliminates the $ characters and return the retrasnformed sequence"""
        sequence_bwt = ""
        for i in reconstruction:
            if i[-1] == "$": # search for the sequence with the $ chraracter at the end
                sequence_bwt = i
        self.results_matrix_construction.append(sequence_bwt) # enter the previusly create sequence into the results list matrix
        sequence_bwt_clean = sequence_bwt[:-1] # eliminates de $ character from the sequence 
        self.result = sequence_bwt_clean # save the sequence as the final result 
        self.results_matrix_construction.append(sequence_bwt_clean)# enter the previusly create sequence into the results list matrix
        return sequence_bwt_clean
    
    def save_sequence_to_file(self, save_path,result):
      """ recover the savinf path and creates a file with the given sequence"""
      save_path.write(result)
      save_path.close()
